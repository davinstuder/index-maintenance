use [master]
set nocount on
go

declare
	@NumberOfWorkers int,
	@Databases nvarchar(max),
	@TerminationType nvarchar(max),
	@TerminationDurationMinutes int,
	@TerminationTime time,
	@ResetQueue nvarchar(max),
	@HistoryRetentionDays int,
	@CreateControllerJob nvarchar(max),
	@OverwriteControllerJob nvarchar(max)

/**********************************************************************************************************************************************************************************************************/
/*                                                                                           Setup Instructions                                                                                           */
/**********************************************************************************************************************************************************************************************************/
/* This installer script will create a number of objects related to this index maintenance solution. These will be created in the master database. It will also create a SQL Agent controller job.        */
/* The controller job will need to be manually scheduled.                                                                                                                                                 */
/*                                                                                                                                                                                                        */
/* SQL Objects Created:                                                                                                                                                                                   */
/* -- Tables and Views --                                                                                                                                                                                 */
/* master.dbo.UpdateIndexesQueue				- This table is a queue of the indexes currenlty being defragmented.                                                                                      */
/* master.dbo.UpdateIndexesOperationOverride	- This table can be used to statically override the defragmentation operation for a given index to force it to use either a reorganize or a rebuild       */
/*												  regardless of the current index fragmentation and the values specified for @FragmentationLevel1 and @FragmentationLevel2 in the controller job. Indexes */
/*												  that you would like to specify the defragmentation level for must be manually added to this table. Acceptable values for the operation column are       */
/*												  REORGANIZE or REBUILD.                                                                                                                                  */
/* master.dbo.UpdateIndexesSelectionOverride	- This table can be used to make sure that any indexes added to it are defragmented regardless of the current fragmentation amount or page count.         */
/* master.dbo.UpdateIndexesEvents				- This table contains events such as installation of the scripts, queue availability depletion, etc.                                                      */
/* master.dbo.vUpdateIndexesHistoricalAverage	- This view is used in the sp_UpdateIndexesWorker stored procedure to get the order with witch to defragment indexes (longest ones first). It was created */
/*												  as a view for the convenience of manually querying.                                                                                                     */
/*                                                                                                                                                                                                        */
/* -- Functions and Stored Procedures --                                                                                                                                                                  */
/* master.dbo.fn_SelectDatabases				- This table valued function will return a list of valid database names based on a supplied comma separated list of values. This is used in the           */
/*												  controller stored procedure.                                                                                                                            */
/* master.dbo.fn_XMLEscape						- This scalar function will take a string and escape out any reserved XML characters in the string. This is used for making sure that data added to the   */
/*												  event_details column in the UpdateIndexesEvents table is propely formatted XML.                                                                         */
/* master.dbo.sp_UpdateIndexesController		- This stored procedure is the index maintenance controller and it will start the worker jobs.                                                            */
/* master.dbo.sp_UpdateIndexesWorker			- This is the stored procedure that the dynamically created worker jobs will run.                                                                         */
/* master.dbo.sp_UpdateIndexesAddEvent			- This stored procedure is used to add entries to the UpdateIndexesEvents table.                                                                          */
/* master.dbo.sp_UpdateIndexesStop				- This stored procedure will signal to the controller and workers to finish the current defragmentations and then stop processing.                        */
/* master.dbo.sp_UpdateIndexesWorkerCleanup		- This stored procedure is used to remove the dynamically created worker jobs.                                                                            */
/*                                                                                                                                                                                                        */
/* Editable Parameters:                                                                                                                                                                                   */
/* @NumberOfWorkers					- This is the number of SQL Agent worker jobs that will be dynamically spawned by the controller job.                                                                 */
/* @Databases						- This is a comma separated list of databases to do index maintenance on. You can supply a static list of database names, one or more dynamic values (see below), or  */
/*									  a statically negated list of database names. The acceptable dynamic values are ALL_DATABASES, SYSTEM_DATABASES, and USER_DATABASES. See below examples.             */
/*									  Example 1: SYSTEM_DATABASES,database1,database2                                                                                                                     */
/*									  Example 2: ALL_DATABASES,-database2,-master                                                                                                                         */
/*									  Example 3: -database2,SYSTEM_DATABASES,USER_DATABASES,-master                                                                                                       */
/*									  Example 4: SYSTEM_DATABASES,USER_DATABASES                                                                                                                          */
/*									  Example 5: ALL_DATABASES                                                                                                                                            */
/* @TerminationType					- The controller job can terminate at a specific time or after a specified number of minutes, thereby signaling to the workers to stop processing after finishing     */
/*									  their current index. Acceptable values are TIME or DURATION.                                                                                                        */
/* @TerminationDurationMinutes		- This is only valid if the @TerminationType is set to DURATION. The controller job will stop after the number of minutes specified.                                  */
/* @TerminationTime					- This is only valid if the @TerminationType is set to TIME. The controller job will stop at the specified time. The specified time should be in the format HH:MM.    */
/* @ResetQueue						- If the last time the controller job started it did not finish all the indexes, should the queue be reset or should maintenance resume where the previous iteration  */
/*									  left off? Acceptable values are Y or N.                                                                                                                             */
/* @HistoryRetentionDays			- How many days of history should be kept in the master.dbo.UpdateIndexesQueue table? NULL value equals no history cleanup and all data will be kept.  If a value is  */
/*									- set it must be greater than or equal to 1.                                                                                                                          */
/* @CreateControllerJob				- Should this installer script create the SQL Agent controller job? Acceptable values are Y or N.                                                                     */
/* @OverwriteControllerJob			- Should this installer script overwrite the SQL Agent controller job if it exists? Acceptable values are Y or N.                                                     */
/*                                                                                                                                                                                                        */
/*                                                                                                                                                                                         Version: 1.4.0 */
/**********************************************************************************************************************************************************************************************************/

set @NumberOfWorkers				= '6'				-- The number of worker jobs that will be spawned by the controller
set @Databases						= 'ALL_DATABASES'	-- See above examples
set @TerminationType				= 'TIME'			-- TIME or DURATION
set @TerminationDurationMinutes		= '480'				-- Termination duration in minutes
set @TerminationTime				= '23:30'			-- Termination time
set @ResetQueue						= 'N'				-- Y or N
set @HistoryRetentionDays			= '90'				-- Days of retention - NULL = no history cleanup
set @CreateControllerJob			= 'Y'				-- Y or N
set @OverwriteControllerJob			= 'N'				-- Y or N

/**********************************************************************************************************************************************************************************************************/
/* Modify below at your own risk. You can edit the above parameters safely and I encourage you to do so to set this up for your environment, but what follows this is the guts of this process. If you    */
/* know what you are doing go ahead. Otherwise, if by editing the below script you blow up your server, cause a disruption in the space-time continuum, or bring on the zombie apocalypse I assume no     */
/* responsibility for the outcome.                                                                                                                                                                        */
/**********************************************************************************************************************************************************************************************************/

/********************************************************************************************/
/*                              Create install param temp table                             */
/********************************************************************************************/
if object_id(N'[tempdb]..[##IndexMaintenanceInstallParams]') is not null begin
	drop table ##IndexMaintenanceInstallParams
end

create table ##IndexMaintenanceInstallParams (
	param_key nvarchar(255),
	param_value nvarchar(max)
)
insert into ##IndexMaintenanceInstallParams values('NumberOfWorkers', @NumberOfWorkers)
insert into ##IndexMaintenanceInstallParams values('Databases', @Databases)
insert into ##IndexMaintenanceInstallParams values('TerminationType', @TerminationType)
insert into ##IndexMaintenanceInstallParams values('TerminationDurationMinutes', @TerminationDurationMinutes)
insert into ##IndexMaintenanceInstallParams values('TerminationTime', @TerminationTime)
insert into ##IndexMaintenanceInstallParams values('HistoryRetentionDays', @HistoryRetentionDays)
insert into ##IndexMaintenanceInstallParams values('ResetQueue', @ResetQueue)
insert into ##IndexMaintenanceInstallParams values('CreateControllerJob', @CreateControllerJob)
insert into ##IndexMaintenanceInstallParams values('OverwriteControllerJob', @OverwriteControllerJob)

/********************************************************************************************/
/*                                Create Add Event Procedure                                */
/********************************************************************************************/
set ansi_nulls on
go
set quoted_identifier on
go
if not exists (select * from sys.objects where object_id = object_id(N'[master].[dbo].[sp_UpdateIndexesAddEvent]') and type in (N'P', N'PC')) begin
	exec dbo.sp_executesql N'create procedure [dbo].[sp_UpdateIndexesAddEvent] as'
end
go
alter procedure [dbo].[sp_UpdateIndexesAddEvent]
	@EventJob nvarchar(50),
	@EventType nvarchar(50),
	@EventDescription nvarchar(50),
	@EventDetails xml = null
as
begin
	/***********************************************************************************************/
	/* Author:		Davin Studer                                                                   */
	/* Description:	This will add an event to the UpdateIndexesEvents table.                       */
	/* Version:		1.4.0                                                                          */
	/***********************************************************************************************/

	set nocount on;
	
	insert into UpdateIndexesEvents (event_time, event_job, event_type, event_description, event_details) values (getdate(), @EventJob, @EventType, @EventDescription, @EventDetails)
end
go

/********************************************************************************************/
/*                             Create function to escape XML                                */
/********************************************************************************************/
set ansi_nulls on
go
set quoted_identifier on
go
if exists (select * from sys.objects where object_id = object_id(N'[master].[dbo].[fn_XMLEscape]') and type in (N'FN')) begin
	drop function [dbo].[fn_XMLEscape]
end
go
create function [dbo].[fn_XMLEscape]
(
	@String varchar(max)
)
returns varchar(max)
as
begin
	/***********************************************************************************************/
	/* Author:		Davin Studer                                                                   */
	/* Description:	This function will take a string and escape any reserved XML characters.       */
	/* Version:		1.4.0                                                                          */
	/***********************************************************************************************/
	declare @escaped varchar(max) 
	set @Escaped = @String
	set @Escaped = replace(@Escaped, '&', '&amp;')
	set @Escaped = replace(@Escaped, '''', '&apos;')
	set @Escaped = replace(@Escaped, '"', '&quot;')
	set @Escaped = replace(@Escaped, '>', '&gt;')
	set @Escaped = replace(@Escaped, '<', '&lt;')
	return(@Escaped)
end
go

/********************************************************************************************/
/*                                   Create events table                                    */
/********************************************************************************************/
set ansi_nulls on
go
set quoted_identifier on
go
if not exists (select * from sys.objects where object_id = object_id(N'[master].[dbo].[UpdateIndexesEvents]') and type in ( N'U')) begin
	create table [dbo].[UpdateIndexesEvents](
		[id] [bigint] identity(1,1) NOT NULL,
		[event_time] [datetime] NOT NULL,
		[event_job] [nvarchar](50) NOT NULL,
		[event_type] [nvarchar](50) NOT NULL,
		[event_description] [nvarchar](50) NOT NULL,
		[event_details] [xml] NULL,
	 constraint [PK_UpdateIndexesEvents] primary key clustered 
	(
		[id] asc
	) with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [primary]
	) on [primary] textimage_on [primary]
end
go

/********************************************************************************************/
/*                                      Log instalation                                     */
/********************************************************************************************/
declare
	@NumberOfWorkers nvarchar(max),
	@Databases nvarchar(max),
	@TerminationType nvarchar(max),
	@TerminationDurationMinutes nvarchar(max),
	@TerminationTime nvarchar(max),
	@ResetQueue nvarchar(max),
	@CreateControllerJob nvarchar(max),
	@OverwriteControllerJob nvarchar(max),
	@XML nvarchar(max) = ''

select @NumberOfWorkers = isnull(param_value, '') from ##IndexMaintenanceInstallParams where param_key = 'NumberOfWorkers'
select @Databases = isnull(param_value, '') from ##IndexMaintenanceInstallParams where param_key = 'Databases'
select @TerminationType = isnull(param_value, '') from ##IndexMaintenanceInstallParams where param_key = 'TerminationType'
select @TerminationDurationMinutes = isnull(param_value, '') from ##IndexMaintenanceInstallParams where param_key = 'TerminationDurationMinutes'
select @TerminationTime = isnull(param_value, '') from ##IndexMaintenanceInstallParams where param_key = 'TerminationTime'
select @ResetQueue = isnull(param_value, '') from ##IndexMaintenanceInstallParams where param_key = 'ResetQueue'
select @CreateControllerJob = isnull(param_value, '') from ##IndexMaintenanceInstallParams where param_key = 'CreateControllerJob'
select @OverwriteControllerJob = isnull(param_value, '') from ##IndexMaintenanceInstallParams where param_key = 'OverwriteControllerJob'

set @XML = ''
set @XML = @XML + '<details>'
set @XML = @XML + '	<version>1.4.0</version>'
set @XML = @XML + '	<number_of_workers>' + master.dbo.fn_XMLEscape(@NumberOfWorkers) + '</number_of_workers>'
set @XML = @XML + '	<databases>' + master.dbo.fn_XMLEscape(@Databases) + '</databases>'
set @XML = @XML + '	<termination_type>' + master.dbo.fn_XMLEscape(@TerminationType) + '</termination_type>'
set @XML = @XML + '	<termination_duration_minutes>' + master.dbo.fn_XMLEscape(@TerminationDurationMinutes) + '</termination_duration_minutes>'
set @XML = @XML + '	<termination_time>' + master.dbo.fn_XMLEscape(@TerminationTime) + '</termination_time>'
set @XML = @XML + '	<reset_queue>' + master.dbo.fn_XMLEscape(@ResetQueue) + '</reset_queue>'
set @XML = @XML + '	<create_controller_job>' + master.dbo.fn_XMLEscape(@CreateControllerJob) + '</create_controller_job>'
set @XML = @XML + '	<overwrite_controller_job>' + master.dbo.fn_XMLEscape(@OverwriteControllerJob) + '</overwrite_controller_job>'
set @XML = @XML + '</details>'

exec master.dbo.sp_UpdateIndexesAddEvent 'IndexMaintenanceInstaller', 'INFO', 'Begin installation', @XML
go

/********************************************************************************************/
/*                                    Create queue table                                    */
/********************************************************************************************/
set ansi_nulls on
go
set quoted_identifier on
go
if not exists (select * from sys.objects where object_id = object_id(N'[master].[dbo].[UpdateIndexesQueue]') and type in ( N'U')) begin
	create table [dbo].[UpdateIndexesQueue](
		[id] [bigint] identity(1,1) NOT NULL,
		[database_name] [nvarchar](128) NOT NULL,
		[schema_name] [nvarchar](128) NOT NULL,
		[object_name] [nvarchar](128) NOT NULL,
		[index_name] [nvarchar](128) NOT NULL,
		[object_type] [nvarchar](60) NOT NULL,
		[index_type] [nvarchar](60) NOT NULL,
		[partition_number] [int] NOT NULL,
		[page_count] [bigint] NOT NULL,
		[avg_fragmentation_in_percent] [float] NOT NULL,
		[historical] [bit] NOT NULL,
		[claimed] [bit] NOT NULL,
		[claimed_by] [nvarchar](255) NULL,
		[start_time] [datetime] NULL,
		[end_time] [datetime] NULL,
		[command] [nvarchar](max) NULL,
	 constraint [PK_UpdateIndexesQueue] primary key clustered
	(
		[id] asc
	) with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [primary]
	) on [primary]

	set ansi_padding on
	create nonclustered index [idx_UpdateIndexesQueue_01] on [dbo].[UpdateIndexesQueue]
	(
		[database_name] asc,
		[schema_name] asc,
		[object_name] asc
	)
	include([index_name],[historical],[start_time],[end_time]) with (sort_in_tempdb = off, drop_existing = off, online = off) on [primary]

	set ansi_padding on
	create nonclustered index [idx_UpdateIndexesQueue_02] on [dbo].[UpdateIndexesQueue]
	(
		[historical] asc,
		[claimed] asc,
		[database_name] asc,
		[schema_name] asc,
		[object_name] asc,
		[end_time] asc
	)
	include([index_name],[avg_fragmentation_in_percent]) with (sort_in_tempdb = off, drop_existing = off, online = off) on [primary]

	set ansi_padding on
	create nonclustered index [idx_UpdateIndexesQueue_03] on [dbo].[UpdateIndexesQueue]
	(
		[historical] asc,
		[end_time] asc,
		[database_name] asc,
		[schema_name] asc,
		[object_name] asc,
		[claimed] asc,
		[avg_fragmentation_in_percent] asc
	)
	include([index_name],[start_time]) with (sort_in_tempdb = off, drop_existing = off, online = off) on [primary]

	set ansi_padding on
	create nonclustered index [idx_UpdateIndexesQueue_04] on [dbo].[UpdateIndexesQueue]
	(
		[schema_name] asc,
		[database_name] asc,
		[object_name] asc
	)
	include([id],[index_name],[object_type],[index_type],[partition_number],[page_count],[avg_fragmentation_in_percent],[historical],[claimed],[claimed_by],[start_time],[end_time]) with (sort_in_tempdb = off, drop_existing = off, online = off) on [primary]
end
go

/********************************************************************************************/
/*                             Create operation override table                              */
/********************************************************************************************/
set ansi_nulls on
go
set quoted_identifier on
go
if not exists (select * from sys.objects where object_id = object_id(N'[master].[dbo].[UpdateIndexesOperationOverride]') and type in ( N'U')) begin
	create table [dbo].[UpdateIndexesOperationOverride](
		[id] [bigint] identity(1,1) NOT NULL,
		[database_name] [nvarchar](128) NOT NULL,
		[schema_name] [nvarchar](128) NOT NULL,
		[object_name] [nvarchar](128) NOT NULL,
		[index_name] [nvarchar](128) NOT NULL,
		[operation] [nvarchar](10) NOT NULL,
	 constraint [PK_UpdateIndexesOperationOverride] primary key clustered 
	(
		[id] asc
	) with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [primary]
	) on [primary]
end
go

/********************************************************************************************/
/*                          Create index selection override table                           */
/********************************************************************************************/
set ansi_nulls on
go
set quoted_identifier on
go
if not exists (select * from sys.objects where object_id = object_id(N'[master].[dbo].[UpdateIndexesSelectionOverride]') and type in ( N'U')) begin
	create table [dbo].[UpdateIndexesSelectionOverride](
		[id] [bigint] identity(1,1) NOT NULL,
		[database_name] [nvarchar](128) NOT NULL,
		[schema_name] [nvarchar](128) NOT NULL,
		[object_name] [nvarchar](128) NOT NULL,
		[index_name] [nvarchar](128) NOT NULL,
		[fragmentation_level] [int] NOT NULL,
		[min_pages] [int] NOT NULL,
		[max_pages] [int] NULL,
	 CONSTRAINT [PK_UpdateIndexesSelectionOverride] primary key clustered
	(
		[id] asc
	) with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [primary]
	) on [PRIMARY]
end
go

/********************************************************************************************/
/*                              Create historical timing view                               */
/********************************************************************************************/
set ansi_nulls on
go
set quoted_identifier on
go
if exists (select * from sys.objects where object_id = object_id(N'[master].[dbo].[vUpdateIndexesHistoricalAverage]') and type in ( N'V')) begin
	drop view [dbo].[vUpdateIndexesHistoricalAverage]
end
go
create view dbo.vUpdateIndexesHistoricalAverage as
/*****************************************************************************************************/
/* Author:		Davin Studer                                                                         */
/* Description:	This will return the histrical average of the time it takes to defragment an index.  */
/* Version:		1.4.0                                                                                */
/*****************************************************************************************************/
select [database_name], [schema_name], [object_name], [index_name], avg(datediff(second, start_time, end_time)) avg_duration_seconds
from UpdateIndexesQueue
where
	historical = '1'
	and start_time is not null
	and end_time is not null
group by [database_name], [schema_name], [object_name], [index_name]
go

/********************************************************************************************/
/*                           Create function to select databases                            */
/********************************************************************************************/
set ansi_nulls on
go
set quoted_identifier on
go
if exists (select * from sys.objects where object_id = object_id(N'[master].[dbo].[fn_SelectDatabases]') and type in ( N'TF')) begin
	drop function [dbo].[fn_SelectDatabases]
end
go
create function [dbo].[fn_SelectDatabases]
(
	@Databases nvarchar(max)
)
returns
@RetTable table (
	[database_name] nvarchar(max)
)
as
begin
	/*****************************************************************************************************/
	/* Author:		Davin Studer                                                                         */
	/* Description:	This will return a valid list of database names based on a suppplied static list of  */
	/*				database names, dynamic values, and statically negated database names.               */
	/* Version:		1.4.0                                                                                */
	/*****************************************************************************************************/
	declare
		@TempXML as xml

	declare @SplitDatabases table (
		[database_name] nvarchar(max)
	)

	declare @TempDatabases table (
		[database_name] nvarchar(max)
	)

	/*********************************************************************/
	/* Split @Databases                                                  */
	/*********************************************************************/
	set @TempXML = cast(('<x>' + replace(@Databases, ',', '</x><x>') + '</x>') as xml)
	
	insert into @SplitDatabases
	select N.value('.', 'nvarchar(max)') as value
	from @TempXML.nodes('x') as T(N)
	/*********************************************************************/
	/*                                                                   */
	/*********************************************************************/
	
	/*********************************************************************/
	/* Insert the static list of database names.                         */
	/*********************************************************************/
	insert into @TempDatabases
	select D.name
	from @SplitDatabases S
	inner join sys.databases D on S.database_name = D.name
	where D.state = 0

	delete from @SplitDatabases
	where [database_name] in (
		select [database_name]
		from @TempDatabases
	)
	/*********************************************************************/
	/*                                                                   */
	/*********************************************************************/

	/*********************************************************************/
	/* Insert all the database names.                                    */
	/*********************************************************************/
	if exists(select * from @SplitDatabases where [database_name] = 'ALL_DATABASES') begin
		insert into @TempDatabases
		select D.name
		from sys.databases D
		where
			D.state = 0
			and D.name <> 'tempdb'

		delete from @SplitDatabases
		where [database_name] = 'ALL_DATABASES'
	end
	/*********************************************************************/
	/*                                                                   */
	/*********************************************************************/

	/*********************************************************************/
	/* Insert the system database names.                                 */
	/*********************************************************************/
	if exists(select * from @SplitDatabases where [database_name] = 'SYSTEM_DATABASES') begin
		insert into @TempDatabases values ('master')
		insert into @TempDatabases values ('msdb')
		insert into @TempDatabases values ('model')

		delete from @SplitDatabases
		where [database_name] = 'SYSTEM_DATABASES'
	end
	/*********************************************************************/
	/*                                                                   */
	/*********************************************************************/

	/*********************************************************************/
	/* Insert the user database names.                                   */
	/*********************************************************************/
	if exists(select * from @SplitDatabases where [database_name] = 'USER_DATABASES') begin
		insert into @TempDatabases
		select D.name
		from sys.databases D
		where
			D.state = 0
			and D.database_id > 4

		delete from @SplitDatabases
		where [database_name] = 'USER_DATABASES'
	end
	/*********************************************************************/
	/*                                                                   */
	/*********************************************************************/

	/*********************************************************************/
	/* Remove statically negated database names.                         */
	/*********************************************************************/
	delete from @SplitDatabases
	where [database_name] not like '-%'

	update @SplitDatabases
	set [database_name] = substring([database_name], 2, len([database_name]) - 1)

	delete from @TempDatabases
	where database_name in (
		select [database_name]
		from @SplitDatabases
	)
	/*********************************************************************/
	/*                                                                   */
	/*********************************************************************/

	insert into @RetTable
	select [database_name]
	from @TempDatabases
	group by [database_name]

	return
end
go

/********************************************************************************************/
/*                                  Create Stop Procedure                                   */
/********************************************************************************************/
set ansi_nulls on
go
set quoted_identifier on
go
if not exists (select * from sys.objects where object_id = object_id(N'[master].[dbo].[sp_UpdateIndexesStop]') and type in (N'P', N'PC')) begin
	exec dbo.sp_executesql N'create procedure [dbo].[sp_UpdateIndexesStop] as'
end
go
alter procedure [dbo].[sp_UpdateIndexesStop]
as
begin
	/***********************************************************************************************/
	/* Author:		Davin Studer                                                                   */
	/* Description:	This will drop the ##WorkerOptions table and thus signal to the controller     */
	/*				and workers to stop processing.                                                */
	/* Version:		1.4.0                                                                          */
	/***********************************************************************************************/

	set nocount on;
	
	if object_id('tempdb..##WorkerOptions') is not null begin
		drop table ##WorkerOptions
	end
end
go

/********************************************************************************************/
/*                          Create Worker Cleanup Stored Procedure                          */
/********************************************************************************************/
set ansi_nulls on
go
set quoted_identifier on
go
if not exists (select * from sys.objects where object_id = object_id(N'[master].[dbo].[sp_UpdateIndexesWorkerCleanup]') and type in (N'P', N'PC')) begin
	exec dbo.sp_executesql N'create procedure [dbo].[sp_UpdateIndexesWorkerCleanup] as'
end
go
alter procedure [dbo].[sp_UpdateIndexesWorkerCleanup]
as
begin
	/***********************************************************************************************/
	/* Author:		Davin Studer                                                                   */
	/* Description:	This will remove the worker jobs that exist.                                   */
	/* Version:		1.4.0                                                                          */
	/***********************************************************************************************/

	set nocount on;
	declare
		@ReturnCode int,
		@WorkerJobName varchar(255)

	declare workerCursor cursor for
	select [name]
	from msdb.dbo.sysjobs_view
	where [name] like 'IndexOptimize - Worker %'

	open workerCursor  
    fetch next from workerCursor into @WorkerJobName
	while @@fetch_status = 0  
    begin
		if exists(select j.name AS job_name from msdb.dbo.sysjobactivity ja inner join msdb.dbo.sysjobs j on ja.job_id = j.job_id where ja.session_id = (select top 1 session_id from msdb.dbo.syssessions order by agent_start_date desc) and ja.start_execution_date is not null and ja.stop_execution_date is null and j.[name] = @WorkerJobName) begin
			exec @ReturnCode = msdb.dbo.sp_stop_job @job_name = @WorkerJobName
		end
		exec @ReturnCode = msdb.dbo.sp_delete_job @job_name = @WorkerJobName
		fetch next from workerCursor into @WorkerJobName
    end

	close workerCursor  
    deallocate workerCursor
end
go

/********************************************************************************************/
/*                            Create Controller Stored Procedure                            */
/********************************************************************************************/
set ansi_nulls on
go
set quoted_identifier on
go
if not exists (select * from sys.objects where object_id = object_id(N'[master].[dbo].[sp_UpdateIndexesController]') and type in (N'P', N'PC')) begin
	exec dbo.sp_executesql N'create procedure [dbo].[sp_UpdateIndexesController] as'
end
go
alter procedure [dbo].[sp_UpdateIndexesController]
	@Databases nvarchar(max),
	@TerminationType nvarchar(max) = 'TIME',
	@TerminationDurationMinutes int = '480',
	@TerminationTime time = '23:30',
	@ResetQueue nvarchar(max) = 'N',
	@HistoryRetentionDays int = null,
	@MinNumberOfPages int = 1000,
	@MaxNumberOfPages int = null,
	@FragmentationLevel1 int = 5,
	@FragmentationLevel2 int = 30,
	@LOBCompaction nvarchar(max) = 'Y',
	@SortInTempdb nvarchar(max) = 'Y',
	@Resumable nvarchar(max) = 'N',
	@MaxDOP int = null,
	@FillFactor int = null,
	@NumberOfWorkers int = null,
	@XML nvarchar(max) = ''
as
begin
	/*****************************************************************************************************/
	/* Author:		Davin Studer                                                                         */
	/* Description:	This will start the controller to defragment the indexes of the specified databases. */
	/* Version:		1.4.0                                                                                */
	/*****************************************************************************************************/

	set nocount on;
	/*********************************************************************/
	/* Input validations                                                 */
	/*********************************************************************/
	if @TerminationType not in ('TIME', 'DURATION') begin
		raiserror('Acceptible values for @TerminationType are TIME or DURATION.', 16, 1)
		return
	end
	if @ResetQueue not in ('Y', 'N') begin
		raiserror('Acceptible values for @ResetQueue are Y or N.', 16, 1)
		return
	end
	set @TerminationDurationMinutes = iif(isnull(@TerminationDurationMinutes, 0) = 0, 480, @TerminationDurationMinutes)
	set @TerminationTime = isnull(@TerminationTime, '23:30')
	set @MinNumberOfPages = isnull(@MinNumberOfPages, 1000)
	set @FragmentationLevel1 = isnull(@FragmentationLevel1, 5)
	set @FragmentationLevel2 = isnull(@FragmentationLevel2, 30)
	set @LOBCompaction = isnull(@LOBCompaction, 'Y')
	set @SortInTempdb = isnull(@SortInTempdb, 'N')
	set @Resumable = isnull(@Resumable, 'N')
	/*********************************************************************/
	/*                                                                   */
	/*********************************************************************/

	/*********************************************************************/
	/* Declarations                                                      */
	/*********************************************************************/
	declare
		@StartTime datetime,
		@GenerateQueueStartTime datetime,
		@WorkersStartTime datetime,
		@HistoryCleanupStartTime datetime,
		@CurrentDatabaseName nvarchar(max),
		@CurrentDatabaseID int,
		@DatabasesNotCompleted int,
		@QueueLeft int,
		@SQL nvarchar(max),
		-- used for creating worker jobs --
		@Iterator int = 0,
		@JobNumber int,
		@JobId binary(16),
		@ReturnCode int,
		@WorkerJobName nvarchar(max),
		@StepName nvarchar(max),
		@JobCommand nvarchar(max),
		@Description nvarchar(max),
		@SAAccount nvarchar(128)
		-- used for creating worker jobs --

	declare @TempDatabases table (
		[database_name] nvarchar(max),
		[completed] bit
	)

	-- Make sure the ##WorkerOptions table doesn't exist
	exec master.dbo.sp_UpdateIndexesStop

	-- The ##WorkerOptions table acts as a flag to tell the workers to keep pulling indexes off the queue
	create table ##WorkerOptions (
		[option_key] nvarchar(max),
		[option_value] nvarchar(max)
	)
	
	insert into ##WorkerOptions values ('Continue','Y')

	set @StartTime = getdate()
	/*********************************************************************/
	/*                                                                   */
	/*********************************************************************/

	/*********************************************************************/
	/* Log start time                                                    */
	/*********************************************************************/
	set @XML = ''
	set @XML = @XML + '<details>'
	set @XML = @XML + '	<start_time>' + master.dbo.fn_XMLEscape(convert(varchar, @StartTime, 121)) + '</start_time>'
	set @XML = @XML + '</details>'
				
	exec master.dbo.sp_UpdateIndexesAddEvent 'sp_UpdateIndexesController', 'INFO', 'Controller start', @XML
	/*********************************************************************/
	/*                                                                   */
	/*********************************************************************/

	/*********************************************************************/
	/* Get Databases                                                     */
	/*********************************************************************/
	insert into @TempDatabases
	select [database_name], '0'
	from master.dbo.fn_SelectDatabases(@Databases)
	/*********************************************************************/
	/*                                                                   */
	/*********************************************************************/

	/*********************************************************************/
	/* Stuff still left in the queue from last round?                    */
	/*********************************************************************/
	select @QueueLeft = count(*)
	from master.dbo.UpdateIndexesQueue
	where historical = '0'
		and (claimed = '0'
		or (claimed = '1' and end_time is null))
	/*********************************************************************/
	/*                                                                   */
	/*********************************************************************/

	/*********************************************************************/
	/* Fill UpdateIndexesQueue if empty or @ResetQueue is Y.             */
	/*********************************************************************/
	if @ResetQueue = 'Y' or @QueueLeft = 0 begin
		update master.dbo.UpdateIndexesQueue
		set historical = '1'
		where historical = '0'
		

		/*********************************************************************/
		/* Start generating the queue                                        */
		/*********************************************************************/
		set @GenerateQueueStartTime = getdate()
		
		set @XML = ''
		set @XML = @XML + '<details>'
		set @XML = @XML + '	<start_time>' + master.dbo.fn_XMLEscape(convert(varchar, @GenerateQueueStartTime, 121)) + '</start_time>'
		set @XML = @XML + '</details>'
				
		exec master.dbo.sp_UpdateIndexesAddEvent 'sp_UpdateIndexesController', 'INFO', 'Generate queue start', @XML
		/*********************************************************************/
		/*                                                                   */
		/*********************************************************************/

		while 1 = 1 begin
			/*********************************************************************/
			/* If we are done adding indexes for databases then exit.            */
			/*********************************************************************/
			select @DatabasesNotCompleted = count(*)
			from @TempDatabases
			where completed = 0

			if @DatabasesNotCompleted = 0 begin
				break
			end
			/*********************************************************************/
			/*                                                                   */
			/*********************************************************************/

			/*********************************************************************/
			/* Fill indexes for each database into UpdateIndexesQueue.           */
			/*********************************************************************/
			select top 1 @CurrentDatabaseName = [database_name]
			from @TempDatabases
			where completed = 0

			select @CurrentDatabaseID = database_id
			from sys.databases D
			where name = @CurrentDatabaseName

			set @SQL = ''
			set @SQL = @SQL + 'use ' + @CurrentDatabaseName + ' ' + char(13) + char(10)
			set @SQL = @SQL + 'insert into master.dbo.UpdateIndexesQueue ([database_name],[schema_name],[object_name],index_name,object_type,index_type,partition_number,page_count,avg_fragmentation_in_percent,historical,claimed,claimed_by,start_time,end_time) ' + char(13) + char(10)
			set @SQL = @SQL + 'select ' + char(13) + char(10)
			set @SQL = @SQL + '	''' + @CurrentDatabaseName + ''' AS [database_name], ' + char(13) + char(10)
			set @SQL = @SQL + '	schemas.[name] AS [schema_name], ' + char(13) + char(10)
			set @SQL = @SQL + '	objects.[name] AS [object_name], ' + char(13) + char(10)
			set @SQL = @SQL + '	indexes.[name] AS [index_name], ' + char(13) + char(10)
			set @SQL = @SQL + '	objects.type_desc AS object_type, ' + char(13) + char(10)
			set @SQL = @SQL + '	indexes.type_desc AS index_type, ' + char(13) + char(10)
			set @SQL = @SQL + '	dm_db_index_physical_stats.partition_number AS partition_number, ' + char(13) + char(10)
			set @SQL = @SQL + '	dm_db_index_physical_stats.page_count AS page_count, ' + char(13) + char(10)
			set @SQL = @SQL + '	dm_db_index_physical_stats.avg_fragmentation_in_percent AS avg_fragmentation_in_percent, ' + char(13) + char(10)
			set @SQL = @SQL + '	''0'' as historical, ' + char(13) + char(10)
			set @SQL = @SQL + '	''0'' as claimed, ' + char(13) + char(10)
			set @SQL = @SQL + '	null as claimed_by, ' + char(13) + char(10)
			set @SQL = @SQL + '	null as start_time, ' + char(13) + char(10)
			set @SQL = @SQL + '	null as end_time ' + char(13) + char(10)
			set @SQL = @SQL + 'from sys.dm_db_index_physical_stats (' + cast(@CurrentDatabaseID as nvarchar) + ', NULL, NULL, NULL, ''LIMITED'') dm_db_index_physical_stats ' + char(13) + char(10)
			set @SQL = @SQL + 'inner join sys.indexes indexes on dm_db_index_physical_stats.[object_id] = indexes.[object_id] and dm_db_index_physical_stats.index_id = indexes.index_id ' + char(13) + char(10)
			set @SQL = @SQL + 'inner join sys.objects objects on indexes.[object_id] = objects.[object_id] ' + char(13) + char(10)
			set @SQL = @SQL + 'inner join sys.schemas schemas on objects.[schema_id] = schemas.[schema_id] ' + char(13) + char(10)
			set @SQL = @SQL + 'left outer join master.dbo.UpdateIndexesSelectionOverride se on ''' + @CurrentDatabaseName + ''' COLLATE SQL_Latin1_General_CP1_CI_AS = se.[database_name] COLLATE SQL_Latin1_General_CP1_CI_AS and schemas.[name] COLLATE SQL_Latin1_General_CP1_CI_AS = se.[schema_name] COLLATE SQL_Latin1_General_CP1_CI_AS and objects.[name] COLLATE SQL_Latin1_General_CP1_CI_AS = se.[object_name] COLLATE SQL_Latin1_General_CP1_CI_AS and indexes.[name] COLLATE SQL_Latin1_General_CP1_CI_AS = se.index_name COLLATE SQL_Latin1_General_CP1_CI_AS ' + char(13) + char(10)
			set @SQL = @SQL + 'where ' + char(13) + char(10)
			set @SQL = @SQL + '	objects.[type] IN(''U'',''V'') ' + char(13) + char(10)
			set @SQL = @SQL + '	and objects.is_ms_shipped = 0 ' + char(13) + char(10)
			set @SQL = @SQL + '	and indexes.[type] IN(1,2,3,4) ' + char(13) + char(10)
			set @SQL = @SQL + '	and indexes.is_disabled = 0 ' + char(13) + char(10)
			set @SQL = @SQL + '	and indexes.is_hypothetical = 0 ' + char(13) + char(10)
			set @SQL = @SQL + '	and dm_db_index_physical_stats.alloc_unit_type_desc = ''IN_ROW_DATA'' ' + char(13) + char(10)
			set @SQL = @SQL + '	and dm_db_index_physical_stats.index_level = 0 ' + char(13) + char(10)
			set @SQL = @SQL + '	and ( ' + char(13) + char(10)
			set @SQL = @SQL + '		( ' + char(13) + char(10)
			set @SQL = @SQL + '			dm_db_index_physical_stats.page_count >= ' + cast(@MinNumberOfPages as nvarchar) + ' ' + char(13) + char(10)
			if @MaxNumberOfPages is not null and @MaxNumberOfPages > @MinNumberOfPages begin
				set @SQL = @SQL + '			and dm_db_index_physical_stats.page_count <= ' + cast(@MaxNumberOfPages as nvarchar) + ' ' + char(13) + char(10)
			end
			set @SQL = @SQL + '			and dm_db_index_physical_stats.avg_fragmentation_in_percent > ' + cast(@FragmentationLevel1 as nvarchar) + ' ' + char(13) + char(10)
			set @SQL = @SQL + '		) ' + char(13) + char(10)
			set @SQL = @SQL + '		or ( ' + char(13) + char(10)
			set @SQL = @SQL + '			se.index_name is not null ' + char(13) + char(10)
			set @SQL = @SQL + '			and dm_db_index_physical_stats.page_count >= se.min_pages ' + char(13) + char(10)
			set @SQL = @SQL + '			and ( ' + char(13) + char(10)
			set @SQL = @SQL + '				1 = case ' + char(13) + char(10)
			set @SQL = @SQL + '						when se.max_pages is null or se.max_pages < 0 or se.max_pages = '''' then 1 ' + char(13) + char(10)
			set @SQL = @SQL + '						when se.max_pages is not null and dm_db_index_physical_stats.page_count <= se.max_pages then 1 ' + char(13) + char(10)
			set @SQL = @SQL + '						else 0 ' + char(13) + char(10)
			set @SQL = @SQL + '				end ' + char(13) + char(10)
			set @SQL = @SQL + '			) ' + char(13) + char(10)
			set @SQL = @SQL + '			and dm_db_index_physical_stats.avg_fragmentation_in_percent >= se.fragmentation_level ' + char(13) + char(10)
			set @SQL = @SQL + '		) ' + char(13) + char(10)
			set @SQL = @SQL + '	) ' + char(13) + char(10)

			begin try
				exec(@SQL)
			end try
			begin catch
				set @XML = ''
				set @XML = @XML + '<details>'
				set @XML = @XML + ' <database_name>' + master.dbo.fn_XMLEscape(isnull(@CurrentDatabaseName, '')) + '</database_name>'
				set @XML = @XML + ' <error>'
				set @XML = @XML + '		<error_number>' + master.dbo.fn_XMLEscape(isnull(cast(ERROR_NUMBER() as nvarchar), '')) + '</error_number>'
				set @XML = @XML + '		<error_severity>' + master.dbo.fn_XMLEscape(isnull(cast(ERROR_SEVERITY() as nvarchar), '')) + '</error_severity>'
				set @XML = @XML + '		<error_state>' + master.dbo.fn_XMLEscape(isnull(cast(ERROR_STATE() as nvarchar), '')) + '</error_state>'
				set @XML = @XML + '		<error_procedure>' + master.dbo.fn_XMLEscape(isnull(ERROR_PROCEDURE(), '')) + '</error_procedure>'
				set @XML = @XML + '		<error_line>' + master.dbo.fn_XMLEscape(isnull(cast(ERROR_LINE() as nvarchar), '')) + '</error_line>'
				set @XML = @XML + '		<error_message>' + master.dbo.fn_XMLEscape(isnull(ERROR_MESSAGE(), '')) + '</error_message>'
				set @XML = @XML + '	</error>'
				set @XML = @XML + '</details>'

				exec master.dbo.sp_UpdateIndexesAddEvent 'sp_UpdateIndexesController', 'ERROR', 'Generate queue', @XML
			end catch

			update @TempDatabases
			set completed = 1
			where [database_name] = @CurrentDatabaseName
			/*********************************************************************/
			/*                                                                   */
			/*********************************************************************/
		end
	
		/*********************************************************************/
		/* End generating the queue                                          */
		/*********************************************************************/
		set @XML = ''
		set @XML = @XML + '<details>'
		set @XML = @XML + '	<start_time>' + master.dbo.fn_XMLEscape(convert(varchar, @GenerateQueueStartTime, 121)) + '</start_time>'
		set @XML = @XML + '	<end_time>' + master.dbo.fn_XMLEscape(convert(varchar, getdate(), 121)) + '</end_time>'
		set @XML = @XML + '</details>'
				
		exec master.dbo.sp_UpdateIndexesAddEvent 'sp_UpdateIndexesController', 'INFO', 'Generate queue end', @XML
		/*********************************************************************/
		/*                                                                   */
		/*********************************************************************/

	end
	else if @ResetQueue = 'N' begin
		/*********************************************************************/
		/* Reset entries claimed but not finished on any previous iteration. */
		/*********************************************************************/
		update master.dbo.UpdateIndexesQueue
		set claimed = '0', claimed_by = null, start_time = null
		where historical = '0'
			and claimed = '1'
			and end_time is null
		/*********************************************************************/
		/*                                                                   */
		/*********************************************************************/
	end
	/*********************************************************************/
	/*                                                                   */
	/*********************************************************************/

	/*********************************************************************/
	/* Remove the worker jobs if any are still around from the last run  */
	/*********************************************************************/
	exec master.dbo.sp_UpdateIndexesWorkerCleanup
	/*********************************************************************/
	/*                                                                   */
	/*********************************************************************/

	/*********************************************************************/
	/* Create & Start the worker jobs.                                   */
	/*********************************************************************/
	set @WorkersStartTime = getdate()

	set @XML = ''
	set @XML = @XML + '<details>'
	set @XML = @XML + '	<start_time>' + master.dbo.fn_XMLEscape(convert(varchar, @WorkersStartTime, 121)) + '</start_time>'
	set @XML = @XML + '</details>'
				
	exec master.dbo.sp_UpdateIndexesAddEvent 'sp_UpdateIndexesController', 'INFO', 'Workers start', @XML

	set @Iterator = 0
	while @Iterator < @NumberOfWorkers begin
		set @JobNumber = @Iterator + 1
		
		set @WorkerJobName = 'IndexOptimize - Worker ' + cast(@JobNumber as nvarchar)
			
		-- Worker does not exist create it
		if not exists(select job_id from msdb.dbo.sysjobs_view where name = @WorkerJobName) begin
			select @SAAccount = name from sys.syslogins where [sid] = 0x01
			set @Description = 'Source: https://iamdav.in' + char(13) + char(10) + 'YOU DO NOT NEED TO SCHEDULE THIS JOB. IT IS CREATED DYNAMICALLY AND STARTED BY THE CONTROLLER JOB. IT WILL BE REMOVED ONCE THE CONTROLLER JOB HAS FINISHED.'
			set @JobId = null
			set @StepName = 'Worker ' +  cast(@JobNumber as nvarchar)
			set @JobCommand = N'exec master.dbo.sp_UpdateIndexesWorker' + char(13) + char(10)
			set @JobCommand = @JobCommand + char(9) + '@FragmentationLevel1 = ''' + cast(@FragmentationLevel1 as nvarchar) + ''',' + char(13) + char(10)
			set @JobCommand = @JobCommand + char(9) + '@FragmentationLevel2 = ''' + cast(@FragmentationLevel2 as nvarchar) + ''',' + char(13) + char(10)
			set @JobCommand = @JobCommand + char(9) + '@LOBCompaction = ''' + @LOBCompaction + ''',' + char(13) + char(10)
			set @JobCommand = @JobCommand + char(9) + '@SortInTempdb = ''' + @SortInTempdb + ''',' + char(13) + char(10)
			set @JobCommand = @JobCommand + char(9) + '@Resumable = ''' + @Resumable + ''',' + char(13) + char(10)
			if @MaxDOP is not null begin
				set @JobCommand = @JobCommand + char(9) + '@MaxDOP = ''' + cast(@MaxDOP as nvarchar) + ''',' + char(13) + char(10)
			end
			if @FillFactor is not null begin
				set @JobCommand = @JobCommand + char(9) + '@FillFactor = ''' + cast(@FillFactor as nvarchar) + ''',' + char(13) + char(10)
			end
			set @JobCommand = @JobCommand + char(9) + '@ClaimedBy = ''' + @WorkerJobName + '''' + char(13) + char(10)

			exec @ReturnCode = msdb.dbo.sp_add_job @job_name = @WorkerJobName, @enabled = 1, @notify_level_eventlog = 0, @notify_level_email = 0, @notify_level_netsend = 0, @notify_level_page = 0, @delete_level = 0, @description = @Description, @category_name = N'[Uncategorized (Local)]', @owner_login_name = @SAAccount, @job_id = @JobId OUTPUT
			exec @ReturnCode = msdb.dbo.sp_add_jobstep @job_id = @JobId, @step_name = @StepName, @step_id = 1, @cmdexec_success_code = 0, @on_success_action = 1, @on_success_step_id = 0, @on_fail_action = 2, @on_fail_step_id = 0, @retry_attempts = 0, @retry_interval = 0, @os_run_priority = 0, @subsystem = N'TSQL', @command = @JobCommand, @database_name = N'master', @flags = 0
			exec @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @JobId, @server_name = N'(local)'
			
			-- Start worker job
			exec msdb..sp_start_job @job_name = @WorkerJobName;
		end

		waitfor delay '00:00:01';
		set @Iterator = @Iterator + 1
	end
	waitfor delay '00:00:05';
	/*********************************************************************/
	/*                                                                   */
	/*********************************************************************/

	/**********************************************************************/
	/* Monitor the queue and exit when it is done, it's time to exit, or  */
	/* the ##WorkerOptions has been manually dropped.                     */
	/**********************************************************************/
	while 1 = 1 begin
		select @QueueLeft = count(*)
		from master.dbo.UpdateIndexesQueue
		where historical = '0'
			and claimed = '0'

		if object_id('tempdb..##WorkerOptions') is null begin
			break
		end
		
		if @QueueLeft = 0 begin
			break
		end
		else begin
			if @TerminationType = 'TIME' and cast(getdate() as time) >= @TerminationTime begin
				break
			end
			else if @TerminationType = 'DURATION' and datediff(minute, @StartTime, getdate()) >= @TerminationDurationMinutes begin
				break
			end
		end

		waitfor delay '00:01:00';
	end
	/*********************************************************************/
	/*                                                                   */
	/*********************************************************************/

	/*********************************************************************/
	/* Drop the tempdb..##WorkerOptions if the process ends normally via */
	/* all the indexes done being defragmented or the termination time   */
	/* or duration is reached.                                           */
	/*********************************************************************/
	exec master.dbo.sp_UpdateIndexesStop
	/*********************************************************************/
	/*                                                                   */
	/*********************************************************************/

	/*********************************************************************/
	/* Wait till all the workers have stopped.                           */
	/*********************************************************************/
	while 1 = 1 begin
		if not exists(select j.name AS job_name from msdb.dbo.sysjobactivity ja inner join msdb.dbo.sysjobs j on ja.job_id = j.job_id where ja.session_id = (select top 1 session_id from msdb.dbo.syssessions order by agent_start_date desc) and ja.start_execution_date is not null and ja.stop_execution_date is null and j.[name] like 'IndexOptimize - Worker %') begin
			break
		end

		waitfor delay '00:00:30';
	end

	set @XML = ''
	set @XML = @XML + '<details>'
	set @XML = @XML + '	<start_time>' + master.dbo.fn_XMLEscape(convert(varchar, @WorkersStartTime, 121)) + '</start_time>'
	set @XML = @XML + '	<end_time>' + master.dbo.fn_XMLEscape(convert(varchar, getdate(), 121)) + '</end_time>'
	set @XML = @XML + '</details>'
				
	exec master.dbo.sp_UpdateIndexesAddEvent 'sp_UpdateIndexesController', 'INFO', 'Workers end', @XML
	/*********************************************************************/
	/*                                                                   */
	/*********************************************************************/

	/*********************************************************************/
	/*  History cleanup                                                  */
	/*********************************************************************/
	if @HistoryRetentionDays is not null and @HistoryRetentionDays >= 1 begin
		set @XML = ''
		set @XML = @XML + '<details>'
		set @XML = @XML + '	<start_time>' + master.dbo.fn_XMLEscape(convert(varchar, @HistoryCleanupStartTime, 121)) + '</start_time>'
		set @XML = @XML + '</details>'
				
		exec master.dbo.sp_UpdateIndexesAddEvent 'sp_UpdateIndexesController', 'INFO', 'History cleanup start', @XML

		delete from master.dbo.UpdateIndexesQueue
		where start_time < dateadd(day, datediff(day, 0, getdate()), 0) - (@HistoryRetentionDays - 1)
			and historical = '1'

		delete from master.dbo.UpdateIndexesEvents
		where event_time < dateadd(day, datediff(day, 0, getdate()), 0) - (@HistoryRetentionDays - 1)


		set @XML = ''
		set @XML = @XML + '<details>'
		set @XML = @XML + '	<start_time>' + master.dbo.fn_XMLEscape(convert(varchar, @HistoryCleanupStartTime, 121)) + '</start_time>'
		set @XML = @XML + '	<end_time>' + master.dbo.fn_XMLEscape(convert(varchar, getdate(), 121)) + '</end_time>'
		set @XML = @XML + '</details>'
				
		exec master.dbo.sp_UpdateIndexesAddEvent 'sp_UpdateIndexesController', 'INFO', 'History cleanup end', @XML
	end
	/*********************************************************************/
	/*                                                                   */
	/*********************************************************************/
	
	/*********************************************************************/
	/* Remove the worker jobs                                            */
	/*********************************************************************/
	exec master.dbo.sp_UpdateIndexesWorkerCleanup
	/*********************************************************************/
	/*                                                                   */
	/*********************************************************************/

	/*********************************************************************/
	/* Log end time                                                      */
	/*********************************************************************/
	set @XML = ''
	set @XML = @XML + '<details>'
	set @XML = @XML + '	<start_time>' + master.dbo.fn_XMLEscape(convert(varchar, @StartTime, 121)) + '</start_time>'
	set @XML = @XML + '	<end_time>' + master.dbo.fn_XMLEscape(convert(varchar, getdate(), 121)) + '</end_time>'
	set @XML = @XML + '</details>'
				
	exec master.dbo.sp_UpdateIndexesAddEvent 'sp_UpdateIndexesController', 'INFO', 'Controller end', @XML
	/*********************************************************************/
	/*                                                                   */
	/*********************************************************************/
end
go

/********************************************************************************************/
/*                              Create Worker Stored Procedure                              */
/********************************************************************************************/
set ansi_nulls on
go
set quoted_identifier on
go
if not exists (select * from sys.objects where object_id = object_id(N'[master].[dbo].[sp_UpdateIndexesWorker]') and type in (N'P', N'PC')) begin
	exec dbo.sp_executesql N'create procedure [dbo].[sp_UpdateIndexesWorker] as'
end
go
alter procedure [dbo].[sp_UpdateIndexesWorker]
	@ClaimedBy nvarchar(255) = null,
	@FragmentationLevel1 int = 5,
	@FragmentationLevel2 int = 30,
	@LOBCompaction nvarchar(max) = 'Y',
	@SortInTempdb nvarchar(max) = 'Y',
	@Resumable nvarchar(max) = 'N',
	@MaxDOP int = null,
	@FillFactor int = null
as
begin
	/***********************************************************************************************/
	/* Author:		Davin Studer                                                                   */
	/* Description:	This will start a worker that will continue to process until there are no more */
	/*				indexes in the queue or the ##WorkerOptions temp table created by the          */
	/*				controller is dropped.                                                         */
	/* Version:		1.4.0                                                                          */
	/***********************************************************************************************/

	set nocount on;
	declare
		@StartTime datetime,
		@CurrentDatabase sysname,
		@CurrentDatabaseCompatibilityLevel int,
		@CurrentSchema sysname,
		@CurrentTable sysname,
		@CurrentIndex sysname,
		@IndexExists bit,
		@CurrentFragmentation float,
		@CurrentOperation nvarchar(10),
		@SQL nvarchar(max),
		@AlterRebuildArgs nvarchar(max),
		@AlterReoganizeArgs nvarchar(max),
		@Now datetime,
		@HighestCompatibilityLevel int,
		@Continue bit = 1,
		@XML nvarchar(max) = '',
		@CheckOnlineSQL nvarchar(max) = '',
		@IsImageText bit,
		@IsFileStream bit
	
	declare @CheckOnlineSQLTable table (
		SchemaID int,
		SchemaName nvarchar(max),
		ObjectID int,
		ObjectName nvarchar(max),
		IndexID int,
		IndexName nvarchar(max),
		IsImageText bit,
		IsFileStream bit
	)

	set @StartTime = getdate()
	
	select @HighestCompatibilityLevel = left(cast(serverproperty('ProductVersion') as nvarchar), charindex('.', cast(serverproperty('ProductVersion') as nvarchar)) - 1) * 10

	/*********************************************************************/
	/* Set Defaults                                                      */
	/*********************************************************************/
	if @ClaimedBy is null begin
		set @ClaimedBy = 'IndexOptimize - Worker'
	end
	/*********************************************************************/
	/*                                                                   */
	/*********************************************************************/

	/*********************************************************************/
	/* Log start time                                                    */
	/*********************************************************************/
	set @XML = ''
	set @XML = @XML + '<details>'
	set @XML = @XML + '	<worker>' + master.dbo.fn_XMLEscape(isnull(@ClaimedBy, '')) + '</worker>'
	set @XML = @XML + '	<start_time>' + master.dbo.fn_XMLEscape(convert(varchar, @StartTime, 121)) + '</start_time>'
	set @XML = @XML + '</details>'
				
	exec master.dbo.sp_UpdateIndexesAddEvent 'sp_UpdateIndexesWorker', 'INFO', 'Worker start', @XML
	/*********************************************************************/
	/*                                                                   */
	/*********************************************************************/

	/*********************************************************************/
	/* Input validations                                                 */
	/*********************************************************************/
	set @FragmentationLevel1 = isnull(@FragmentationLevel1, 5)
	set @FragmentationLevel2 = isnull(@FragmentationLevel2, 30)
	set @LOBCompaction = isnull(@LOBCompaction, 'Y')
	set @SortInTempdb = isnull(@SortInTempdb, 'N')
	set @Resumable = isnull(@Resumable, 'N')
	/*********************************************************************/
	/*                                                                   */
	/*********************************************************************/

	/*********************************************************************/
	/* Translate values                                                  */
	/*********************************************************************/
	set @LOBCompaction = iif(@LOBCompaction = 'Y', 'on', 'off')
	set @SortInTempdb = iif(@SortInTempdb = 'Y', 'on', 'off')
	set @Resumable = iif(@Resumable = 'Y', 'on', 'off')
	/*********************************************************************/
	/*                                                                   */
	/*********************************************************************/
	
	/*********************************************************************/
	/* Keep looping through the queue and grabbing the top one till      */
	/* there are no longer indexes in the queue that have not been       */
	/* claimed.                                                          */
	/*********************************************************************/
	while (object_id('tempdb..##WorkerOptions') is not null) begin
		/*********************************************************************/
		/* Pull the top index off the queue ordered by average historical    */
		/* defragmentation time descending. Also, don't get an index for a   */
		/* table that currenlty has an index defragmenting. This process     */
		/* will lock the table so that only one worker at a time may grab an */
		/* index off the queue.                                              */
		/*********************************************************************/
		begin transaction
		begin try
			update master.dbo.UpdateIndexesQueue
			set @CurrentDatabase = Q.[database_name],
				@CurrentSchema = Q.[schema_name],
				@CurrentTable = Q.[object_name],
				@CurrentIndex = Q.[index_name],
				@CurrentFragmentation = Q.[avg_fragmentation_in_percent],
				[claimed] = '1',
				[claimed_by] = @ClaimedBy,
				[start_time] = getdate(),
				@CurrentOperation = O.[operation]
			from master.dbo.UpdateIndexesQueue Q with (tablockx, holdlock) -- This does the table r/w locking
			left outer join master.dbo.UpdateIndexesOperationOverride O on
				Q.[database_name] = O.[database_name]
				and Q.[schema_name] = O.[schema_name]
				and Q.[object_name] = O.[object_name]
				and Q.[index_name] = O.[index_name]
			inner join (
				/*********************************************************************/
				/* Grab the first index that has not been claimed and is not on a    */
				/* table that currently has an index being defragmented. This list   */
				/* is ordered by the average time it takes to defragment that index  */
				/* descending, so that the ones that take the longest to defragment  */
				/* get started first.                                                */
				/*********************************************************************/
				select top 1 MQ.*
				from master.dbo.UpdateIndexesQueue MQ
				left outer join master.dbo.vUpdateIndexesHistoricalAverage H on
					MQ.[database_name] = H.[database_name]
					and MQ.[schema_name] = H.[schema_name]
					and MQ.[object_name] = H.[object_name]
					and MQ.[index_name] = H.[index_name]
				where
					MQ.historical = '0'
					and MQ.claimed = '0'
					and MQ.[database_name] + '.' + MQ.[schema_name] + '.' + MQ.[object_name] not in (
						/*********************************************************************/
						/* These tables currently have an index being defragmented.          */
						/*********************************************************************/
						select  [database_name] + '.' + [schema_name] + '.' + [object_name]
						from master.dbo.UpdateIndexesQueue
						where
							historical = '0'
							and [claimed] = '1'
							and [end_time] is null
						/*********************************************************************/
						/*                                                                   */
						/*********************************************************************/
					)
				order by isnull(H.avg_duration_seconds, 0) desc, MQ.[avg_fragmentation_in_percent] desc
				/*********************************************************************/
				/*                                                                   */
				/*********************************************************************/
			) Q2
			on
				Q.[database_name] = Q2.[database_name]
				and Q.[schema_name] = Q2.[schema_name]
				and Q.[object_name] = Q2.[object_name]
				and Q.[index_name] = Q2.[index_name]
			where Q.[historical] = '0'

			if @@rowcount = 0 begin
				set @Continue = 0
			end
		end try
		begin catch
			set @XML = ''
			set @XML = @XML + '<details>'
			set @XML = @XML + '	<worker>' + master.dbo.fn_XMLEscape(isnull(@ClaimedBy, '')) + '</worker>'
			set @XML = @XML + ' <error>'
			set @XML = @XML + '		<error_number>' + master.dbo.fn_XMLEscape(isnull(cast(ERROR_NUMBER() as nvarchar), '')) + '</error_number>'
			set @XML = @XML + '		<error_severity>' + master.dbo.fn_XMLEscape(isnull(cast(ERROR_SEVERITY() as nvarchar), '')) + '</error_severity>'
			set @XML = @XML + '		<error_state>' + master.dbo.fn_XMLEscape(isnull(cast(ERROR_STATE() as nvarchar), '')) + '</error_state>'
			set @XML = @XML + '		<error_procedure>' + master.dbo.fn_XMLEscape(isnull(ERROR_PROCEDURE(), '')) + '</error_procedure>'
			set @XML = @XML + '		<error_line>' + master.dbo.fn_XMLEscape(isnull(cast(ERROR_LINE() as nvarchar), '')) + '</error_line>'
			set @XML = @XML + '		<error_message>' + master.dbo.fn_XMLEscape(isnull(ERROR_MESSAGE(), '')) + '</error_message>'
			set @XML = @XML + '	</error>'
			set @XML = @XML + '</details>'
			
			if @@trancount > 0 begin
				rollback transaction
			end
			
			exec master.dbo.sp_UpdateIndexesAddEvent 'sp_UpdateIndexesWorker', 'ERROR', 'Claim index', @XML

			goto next_index
		end catch

		if @@trancount > 0 begin
			commit transaction
		end
		/*********************************************************************/
		/*                                                                   */
		/*********************************************************************/

		if @Continue = 0 begin
			/*********************************************************************/
			/* No more indexes left in the queue. Go ahead and stop.             */
			/*********************************************************************/
			set @XML = ''
			set @XML = @XML + '<details>'
			set @XML = @XML + '	<worker>' + master.dbo.fn_XMLEscape(isnull(@ClaimedBy, '')) + '</worker>'
			set @XML = @XML + '</details>'

			exec master.dbo.sp_UpdateIndexesAddEvent 'sp_UpdateIndexesWorker', 'INFO', 'Queue availability depleted', @XML
			break;
			/*********************************************************************/
			/*                                                                   */
			/*********************************************************************/
		end
		else begin
			/*********************************************************************/
			/* Check to see if the index exists or if it was droppped between    */
			/* now and when the queue was created.                               */
			/*********************************************************************/
			if exists (select * from sys.databases where name = @CurrentDatabase) begin
				set @SQL = ''
				set @SQL = @SQL + 'use [' + @CurrentDatabase + ']; select @exists = case when count(*) > 0 then ''1'' else ''0'' end from sys.indexes where name=''' + @CurrentIndex + ''' AND object_id = object_id(''[' + @CurrentSchema + '].[' + @CurrentTable + ']'')'

				exec dbo.sp_executesql @SQL, N'@exists bit output', @exists = @IndexExists output;
			end
			else begin
				set @IndexExists = 0
			end

			-- The index doesn't exist mark it done and move to the next index
			if @IndexExists = 0 begin
				update master.dbo.UpdateIndexesQueue
				set
					end_time = start_time,
					command = 'N/A index doesn''t exist'
				where
					[database_name] = @CurrentDatabase
					and [schema_name] = @CurrentSchema
					and [object_name] = @CurrentTable
					and [index_name] = @CurrentIndex
					and historical = 0
				
				goto next_index
			end
			/*********************************************************************/
			/*                                                                   */
			/*********************************************************************/

			set @SQL = ''

			select @CurrentDatabaseCompatibilityLevel = compatibility_level from sys.databases where name = @CurrentDatabase

			-- Rebuild args

			-- Rebuild online or offline
			set @CheckOnlineSQL = ''
			set @CheckOnlineSQL = @CheckOnlineSQL + 'select' + char(10) + char(13)
			set @CheckOnlineSQL = @CheckOnlineSQL + '	S.[schema_id] as SchemaID,' + char(10) + char(13)
			set @CheckOnlineSQL = @CheckOnlineSQL + '	S.[name] as SchemaName,' + char(10) + char(13)
			set @CheckOnlineSQL = @CheckOnlineSQL + '	O.[object_id] as ObjectID,' + char(10) + char(13)
			set @CheckOnlineSQL = @CheckOnlineSQL + '	O.[name] as ObjectName,' + char(10) + char(13)
			set @CheckOnlineSQL = @CheckOnlineSQL + '	I.index_id as IndexID,' + char(10) + char(13)
			set @CheckOnlineSQL = @CheckOnlineSQL + '	I.[name] as IndexName,' + char(10) + char(13)
			set @CheckOnlineSQL = @CheckOnlineSQL + '	case when I.[type] = 1 and exists (' + char(10) + char(13)
			set @CheckOnlineSQL = @CheckOnlineSQL + '		select *' + char(10) + char(13)
			set @CheckOnlineSQL = @CheckOnlineSQL + '		from [' + @CurrentDatabase + '].sys.columns CO' + char(10) + char(13)
			set @CheckOnlineSQL = @CheckOnlineSQL + '		inner join [' + @CurrentDatabase + '].sys.types TY on CO.system_type_id = TY.user_type_id' + char(10) + char(13)
			set @CheckOnlineSQL = @CheckOnlineSQL + '		where' + char(10) + char(13)
			set @CheckOnlineSQL = @CheckOnlineSQL + '			CO.[object_id] = O.object_id' + char(10) + char(13)
			set @CheckOnlineSQL = @CheckOnlineSQL + '			and TY.name in(''image'', ''text'', ''ntext'')' + char(10) + char(13)
			set @CheckOnlineSQL = @CheckOnlineSQL + '	) then 1 else 0 end as IsImageText,' + char(10) + char(13)
			set @CheckOnlineSQL = @CheckOnlineSQL + '	case when I.[type] = 1 and exists (' + char(10) + char(13)
			set @CheckOnlineSQL = @CheckOnlineSQL + '		select *' + char(10) + char(13)
			set @CheckOnlineSQL = @CheckOnlineSQL + '		from [' + @CurrentDatabase + '].sys.columns CO' + char(10) + char(13)
			set @CheckOnlineSQL = @CheckOnlineSQL + '		where' + char(10) + char(13)
			set @CheckOnlineSQL = @CheckOnlineSQL + '			CO.[object_id] = O.object_id' + char(10) + char(13)
			set @CheckOnlineSQL = @CheckOnlineSQL + '			and CO.is_filestream = 1' + char(10) + char(13)
			set @CheckOnlineSQL = @CheckOnlineSQL + '	) then 1 else 0 end as IsFileStream' + char(10) + char(13)
			set @CheckOnlineSQL = @CheckOnlineSQL + 'from [' + @CurrentDatabase + '].sys.indexes I' + char(10) + char(13)
			set @CheckOnlineSQL = @CheckOnlineSQL + 'inner join [' + @CurrentDatabase + '].sys.objects O on I.[object_id] = O.[object_id]' + char(10) + char(13)
			set @CheckOnlineSQL = @CheckOnlineSQL + 'inner join [' + @CurrentDatabase + '].sys.schemas S on O.[schema_id] = S.[schema_id]' + char(10) + char(13)
			set @CheckOnlineSQL = @CheckOnlineSQL + 'left outer join [' + @CurrentDatabase + '].sys.stats ST on I.[object_id] = ST.[object_id] and I.[index_id] = ST.[stats_id]' + char(10) + char(13)
			set @CheckOnlineSQL = @CheckOnlineSQL + 'where' + char(10) + char(13)
			set @CheckOnlineSQL = @CheckOnlineSQL + '	O.[type] in (''U'', ''V'')' + char(10) + char(13)
			set @CheckOnlineSQL = @CheckOnlineSQL + '	and I.[type] in (1, 2, 3, 4, 5, 6, 7)' + char(10) + char(13)
			set @CheckOnlineSQL = @CheckOnlineSQL + '	and I.is_disabled = 0' + char(10) + char(13)
			set @CheckOnlineSQL = @CheckOnlineSQL + '	and I.is_hypothetical = 0' + char(10) + char(13)
			set @CheckOnlineSQL = @CheckOnlineSQL + '	and S.name = ''' + @CurrentSchema + '''' + char(10) + char(13)
			set @CheckOnlineSQL = @CheckOnlineSQL + '	and O.name = ''' + @CurrentTable + '''' + char(10) + char(13)
			set @CheckOnlineSQL = @CheckOnlineSQL + '	and I.name = ''' + @CurrentIndex + '''' + char(10) + char(13)

			insert into @CheckOnlineSQLTable 
			exec (@CheckOnlineSQL)

			set @IsImageText = 0
			set @IsFileStream = 0
			select @IsImageText = IsImageText, @IsFileStream = IsFileStream from @CheckOnlineSQLTable
			
			set @AlterRebuildArgs = ''
			if @IsImageText = 1 or @IsFileStream = 1 begin
				set @AlterRebuildArgs = @AlterRebuildArgs + 'online = off'
			end
			else begin
				set @AlterRebuildArgs = @AlterRebuildArgs + 'online = on'
			end

			if @HighestCompatibilityLevel >= 140 and @CurrentDatabaseCompatibilityLevel >= 140 begin
				if @Resumable = 'on' begin
					set @SortInTempdb = 'off'
					set @AlterRebuildArgs = @AlterRebuildArgs + ', resumable = ' + @Resumable
				end
			end
			set @AlterRebuildArgs = @AlterRebuildArgs + ', sort_in_tempdb = ' + @SortInTempdb
			if @MaxDOP is not null begin
				set @AlterRebuildArgs = @AlterRebuildArgs + ', maxdop = ' + cast(@MaxDOP as nvarchar)
			end
			if @FillFactor is not null begin
				set @AlterRebuildArgs = @AlterRebuildArgs + ', fillfactor = ' + cast(@FillFactor as nvarchar)
			end

			-- Reorganize args
			set @AlterReoganizeArgs = ''
			set @AlterReoganizeArgs = @AlterReoganizeArgs + 'lob_compaction = ' + @LOBCompaction
			
			if @CurrentFragmentation > @FragmentationLevel2 or @CurrentOperation = 'REBUILD' begin
				set @SQL = @SQL + 'alter index [' + @CurrentIndex + '] on [' + @CurrentDatabase + '].[' + @CurrentSchema + '].[' + @CurrentTable + '] rebuild with (' + @AlterRebuildArgs + ');'
			end
			else if (@CurrentFragmentation <= @FragmentationLevel2) or @CurrentOperation = 'REORGANIZE' begin
				set @SQL = @SQL + 'alter index [' + @CurrentIndex + '] on [' + @CurrentDatabase + '].[' + @CurrentSchema + '].[' + @CurrentTable + '] reorganize with (' + @AlterReoganizeArgs + ');'
			end

			update master.dbo.UpdateIndexesQueue
			set
				command = @SQL
			where
				[database_name] = @CurrentDatabase
				and [schema_name] = @CurrentSchema
				and [object_name] = @CurrentTable
				and [index_name] = @CurrentIndex
				and historical = 0

			set @SQL = @SQL + 'update master.dbo.UpdateIndexesQueue set end_time = getdate() where [database_name] = ''' + @CurrentDatabase + ''' and [schema_name] = ''' + @CurrentSchema + ''' and [object_name] = ''' + @CurrentTable + ''' and [index_name] = ''' + @CurrentIndex + ''' and historical = ''0'';'

			/*********************************************************************/
			/* Try defragmenting the index.                                      */
			/*********************************************************************/
			begin try
				exec(@SQL)
			end try
			begin catch
				set @XML = ''
				set @XML = @XML + '<details>'
				set @XML = @XML + '	<worker>' + master.dbo.fn_XMLEscape(isnull(@ClaimedBy, '')) + '</worker>'
				set @XML = @XML + '	<database>' + master.dbo.fn_XMLEscape(isnull(@CurrentDatabase, '')) + '</database>'
				set @XML = @XML + '	<schema>' + master.dbo.fn_XMLEscape(isnull(@CurrentSchema, '')) + '</schema>'
				set @XML = @XML + '	<table>' + master.dbo.fn_XMLEscape(isnull(@CurrentTable, '')) + '</table>'
				set @XML = @XML + '	<index>' + master.dbo.fn_XMLEscape(isnull(@CurrentIndex, '')) + '</index>'
				set @XML = @XML + ' <error>'
				set @XML = @XML + '		<error_number>' + master.dbo.fn_XMLEscape(isnull(cast(ERROR_NUMBER() as nvarchar), '')) + '</error_number>'
				set @XML = @XML + '		<error_severity>' + master.dbo.fn_XMLEscape(isnull(cast(ERROR_SEVERITY() as nvarchar), '')) + '</error_severity>'
				set @XML = @XML + '		<error_state>' + master.dbo.fn_XMLEscape(isnull(cast(ERROR_STATE() as nvarchar), '')) + '</error_state>'
				set @XML = @XML + '		<error_procedure>' + master.dbo.fn_XMLEscape(isnull(ERROR_PROCEDURE(), '')) + '</error_procedure>'
				set @XML = @XML + '		<error_line>' + master.dbo.fn_XMLEscape(isnull(cast(ERROR_LINE() as nvarchar), '')) + '</error_line>'
				set @XML = @XML + '		<error_message>' + master.dbo.fn_XMLEscape(isnull(ERROR_MESSAGE(), '')) + '</error_message>'
				set @XML = @XML + '	</error>'
				set @XML = @XML + '</details>'
				
				exec master.dbo.sp_UpdateIndexesAddEvent 'sp_UpdateIndexesWorker', 'ERROR', 'Defragmenting index', @XML

				/*********************************************************************/
				/* Something went wrong. Take this index off the queue.              */
				/*********************************************************************/
				update master.dbo.UpdateIndexesQueue
				set
					end_time = start_time,
					command = command + char(13) + char(10) + 'ERROR: ' + isnull(ERROR_MESSAGE(), '')
				where
					[database_name] = @CurrentDatabase
					and [schema_name] = @CurrentSchema
					and [object_name] = @CurrentTable
					and [index_name] = @CurrentIndex
					and historical = '0'
				/*********************************************************************/
				/*                                                                   */
				/*********************************************************************/
			end catch
			/*********************************************************************/
			/*                                                                   */
			/*********************************************************************/
		end					

		next_index:
	end
	/*********************************************************************/
	/*                                                                   */
	/*********************************************************************/

	/*********************************************************************/
	/* Log end time                                                      */
	/*********************************************************************/
	set @XML = ''
	set @XML = @XML + '<details>'
	set @XML = @XML + '	<worker>' + master.dbo.fn_XMLEscape(isnull(@ClaimedBy, '')) + '</worker>'
	set @XML = @XML + '	<start_time>' + master.dbo.fn_XMLEscape(convert(varchar, @StartTime, 121)) + '</start_time>'
	set @XML = @XML + '	<end_time>' + master.dbo.fn_XMLEscape(convert(varchar, getdate(), 121)) + '</end_time>'
	set @XML = @XML + '</details>'
				
	exec master.dbo.sp_UpdateIndexesAddEvent 'sp_UpdateIndexesWorker', 'INFO', 'Worker end', @XML
	/*********************************************************************/
	/*                                                                   */
	/*********************************************************************/
end
go

/********************************************************************************************/
/*                                   Create controller job                                  */
/********************************************************************************************/
declare
	@JobId binary(16),
	@Databases nvarchar(max),
	@TerminationType nvarchar(max),
	@NumberOfWorkers int,
	@TerminationDurationMinutes int,
	@TerminationTime time,
	@HistoryRetentionDays int,
	@ResetQueue nvarchar(max),
	@ReturnCode int,
	@JobCommand nvarchar(max),
	@CreateControllerJob nvarchar(max),
	@OverwriteControllerJob nvarchar(max),
	@SAAccount nvarchar(128)

select @Databases = param_value from ##IndexMaintenanceInstallParams where param_key = 'Databases'
select @TerminationType = param_value from ##IndexMaintenanceInstallParams where param_key = 'TerminationType'
select @NumberOfWorkers = cast(param_value as int) from ##IndexMaintenanceInstallParams where param_key = 'NumberOfWorkers'
select @TerminationDurationMinutes = cast(param_value as int) from ##IndexMaintenanceInstallParams where param_key = 'TerminationDurationMinutes'
select @TerminationTime = cast(param_value as time) from ##IndexMaintenanceInstallParams where param_key = 'TerminationTime'
select @HistoryRetentionDays = param_value from ##IndexMaintenanceInstallParams where param_key = 'HistoryRetentionDays'
select @ResetQueue = param_value from ##IndexMaintenanceInstallParams where param_key = 'ResetQueue'
select @CreateControllerJob = param_value from ##IndexMaintenanceInstallParams where param_key = 'CreateControllerJob'
select @OverwriteControllerJob = param_value from ##IndexMaintenanceInstallParams where param_key = 'OverwriteControllerJob'
select @SAAccount = name from sys.syslogins where [sid] = 0x01

if @CreateControllerJob = 'Y' begin
	set @JobCommand = N'exec master.dbo.sp_UpdateIndexesController
	@Databases = ''' + @Databases  + ''',
	@TerminationType = ''' + @TerminationType + ''',
	@NumberOfWorkers = ''' + cast(@NumberOfWorkers as nvarchar) + ''',' + char(13) + char(10)
	if @TerminationType = 'DURATION' begin
		set @JobCommand = @JobCommand + '	@TerminationDuration = ''' + cast(@TerminationDurationMinutes as nvarchar) + ''',' + char(13) + char(10)
	end
	else if @TerminationType = 'TIME' begin
		set @JobCommand = @JobCommand + '	@TerminationTime = ''' + left(convert(nvarchar, @TerminationTime, 108), 5) + ''',' + char(13) + char(10)
	end
	if @HistoryRetentionDays is not null and @HistoryRetentionDays >= 1 begin
		set @JobCommand = @JobCommand + '	@HistoryRetentionDays = ''' + cast(@HistoryRetentionDays as nvarchar) + ''',' + char(13) + char(10)
	end
	set @JobCommand = @JobCommand + '	@ResetQueue = ''' + @ResetQueue + ''''

	if exists(select job_id from msdb.dbo.sysjobs_view where name = N'IndexOptimize - Controller') and @OverwriteControllerJob = 'Y' begin
		exec @ReturnCode = msdb.dbo.sp_delete_job @job_name = 'IndexOptimize - Controller'
	end

	if not exists(select job_id from msdb.dbo.sysjobs_view where name = N'IndexOptimize - Controller') begin
		exec @ReturnCode = msdb.dbo.sp_add_job @job_name = N'IndexOptimize - Controller', @enabled = 1, @notify_level_eventlog = 0, @notify_level_email = 0, @notify_level_netsend = 0, @notify_level_page = 0, @delete_level = 0, @description = N'Source: https://iamdav.in', @category_name = N'[Uncategorized (Local)]', @owner_login_name = @SAAccount, @job_id = @JobId OUTPUT
		exec @ReturnCode = msdb.dbo.sp_add_jobstep @job_id = @JobId, @step_name = N'Controller', @step_id = 1, @cmdexec_success_code = 0, @on_success_action = 1, @on_success_step_id = 0, @on_fail_action = 2, @on_fail_step_id = 0, @retry_attempts = 0, @retry_interval = 0, @os_run_priority = 0, @subsystem = N'TSQL', @command = @JobCommand, @database_name = N'master', @flags = 0
		exec @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @JobId, @server_name = N'(local)'
	end
end
go

declare
	@CreateControllerJob nvarchar(max)

select @CreateControllerJob = param_value
from ##IndexMaintenanceInstallParams
where param_key = 'CreateControllerJob'

print 'The index maintenance script objects have been created.'

if @CreateControllerJob = 'Y' begin
	print 'The index maintenance SQL Agent controller job has been created if it did not already exist or you wanted it overwritten. The schedule for the ''IndexOptimize - Controller'' job HAS NOT been created. You must manually create the schedule(s) for which you would like to have the maintenance job run.'
end
else begin
	print 'The index maintenance SQL Agent controller job has not been created.'
end
go

/********************************************************************************************/
/*                               Drop install param temp table                              */
/********************************************************************************************/
if object_id(N'[tempdb]..[##IndexMaintenanceInstallParams]') is not null begin
	drop table ##IndexMaintenanceInstallParams
end
go

exec master.dbo.sp_UpdateIndexesAddEvent 'IndexMaintenanceInstaller', 'INFO', 'End installation', '<details><version>1.4.0</version></details>'
go