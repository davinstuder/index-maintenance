# Index Maintenance #
This SQL script is an index maintenance solution for Microsoft SQL server. This main feature of this solution that sets it apart from other solutions such as SQL Maintenance Plans, or [Ola Hallengren's Index Maintenance script](https://ola.hallengren.com/) (which is truly an excellent script) is that it can do index defragmentation in parallel. Most index maintenance solutions do index defragmentation in serial ... meaning one at a time. For smaller databases this may not really be an issue, but when your database grows to terabytes in size defragmenting indexes one at a time can become troublesome.

That is how this script was born. I was using Ola Hallengren's script and had a 4TB database that took over 24 hours to do index defragmentation as the indexes were being defragmented one at a time. I tried to compensate for this by creating multiple jobs and splitting them up based on pages in the table. I also tried to balance the jobs such that they would do about the same about of work, but that became a huge hassle to manage. So, I went online to see if anyone had a script that could do index defragmentation in parallel. I was not able to find one, so I decided to write my own. Here is the fruit of that.
## Current Version ##
1.4.0

## Installing/Upgrading the Solution ##
Simply download the IndexMaintenance.sql installer file and run it on your server to install or upgrade the solution. There are some parameters that you can set prior to running it. The main ones that you would want to set would be...

* @NumberOfWorkers: How many indexes should be defragmented at a time?
* @Databases: Which databases should be defragmented? I used Ola's convention here.
* @TerminationType: How should the job terminate? Based on time of day or duration?
* @TerminationDurationMinutes/@TerminationTime: How you configure @TerminationType will define which you will set.
* @ResetQueue: Should the defragmentation queue be reset each time or should it try and pick up where it left off if it terminated before finishing?
* @HistoryRetentionDays: How many days of history should be kept in the master.dbo.UpdateIndexesQueue table?

## What is created and where? ##
All the objects that are created will be created in the master database. Below is a description of everything that is created.

### Tables and Views ###
**master.dbo.UpdateIndexesQueue**
This table is a queue of the indexes currently being defragmented. It also contains all the historical runs.

**master.dbo.UpdateIndexesOperationOverride**
This table can be used to statically override the defragmentation operation for a given index to force it to use either a reorganize or a rebuild regardless of the current index fragmentation and the values specified for @FragmentationLevel1 and @FragmentationLevel2 in the controller job. Indexes that you would like to specify the defragmentation level for must be manually added to this table. Acceptable values for the operation column are REORGANIZE or REBUILD.

**master.dbo.UpdateIndexesSelectionOverride**
This table can be used to make sure that any indexes added to it are defragmented regardless of the current fragmentation amount or page count.

**master.dbo.UpdateIndexesEvents**
This table contains events such as installation of the scripts, queue availability depletion, etc.

**master.dbo.vUpdateIndexesHistoricalAverage**
This view is used in the sp_UpdateIndexesWorker stored procedure to get the order with witch to defragment indexes (longest ones first). It was created as a view for the convenience of manually querying.

### Functions and Stored Procedures ###
**master.dbo.fn_SelectDatabases**
This table valued function will return a list of valid database names based on a supplied comma separated list of values. This is used in the controller stored procedure.

**master.dbo.fn_XMLEscape**
This scalar function will take a string and escape out any reserved XML characters in the string. This is used for making sure that data added to the event_details column in the UpdateIndexesEvents table is propely formatted XML.

**master.dbo.sp_UpdateIndexesController**
This stored procedure is the index maintenance controller and it will start the worker jobs.

**master.dbo.sp_UpdateIndexesWorker**
This is the stored procedure that the worker jobs will run.

**master.dbo.sp_UpdateIndexesAddEvent**
This stored procedure is used to add entries to the UpdateIndexesEvents table.

**master.dbo.sp_UpdateIndexesStop**
This stored procedure will signal to the controller and workers to finish the current defragmentations and then stop processing.

**master.dbo.sp_UpdateIndexesWorkerCleanup**
This stored procedure is used to remove the dynamically created worker jobs. 

## SQL Agent Jobs ##
**IndexOptimize - Controller**
This job will create the queue of indexes to defragment and will then spawn the worker jobs once the queue is created. The job is created but a schedule is not created. **You must create the schedule manually.**

**IndexOptimize - Worker #**
There will be multiple worker jobs created dynamically at run time by the controller job  based on the value you set for @NumberOfWorkers when you ran the install script. This value can be changed by updating the @NumberOfWorkers parameter in the controller job step. The workers will pull indexes off the queue and defragment them until the queue is depleted or the process termination time/duration is reached. When the controller job finishes these dynamically created jobs will be removed.

## Editable Installer Parameters ##
**@NumberOfWorkers**
This will set the value used in the controller job. See Editable Controller Runtime Parameters below for its description.

**@Databases**
This will set the value used in the controller job. See Editable Controller Runtime Parameters below for its description.

**@TerminationType**
This will set the value used in the controller job. See Editable Controller Runtime Parameters below for its description.

**@TerminationDurationMinutes**
This will set the value used in the controller job. See Editable Controller Runtime Parameters below for its description.

**@TerminationTime**
This will set the value used in the controller job. See Editable Controller Runtime Parameters below for its description.

**@ResetQueue**
This will set the value used in the controller job. See Editable Controller Runtime Parameters below for its description.

**@HistoryRetentionDays**
How many days of history should be kept in the master.dbo.UpdateIndexesQueue table? NULL value equals no history cleanup and all data will be kept. If a value is set it must be greater than or equal to 1.

**@CreateControllerJob**
Should this installer script create the SQL Agent controller job? Acceptable values are Y or N.

**@OverwriteControllerJob**
Should this installer script overwrite the SQL Agent controller job if it exists? Acceptable values are Y or N. Typically you would leave this set to N so that if you make changes to the job then installing an updated version of this solution will not overwrite your changes.

## Editable Controller Runtime Parameters ##
These parameters can be set in the controller job. They control how the solution will run.
**@Databases**
This is a comma separated list of databases to do index maintenance on. You can supply a static list of database names, one or more dynamic values (see below), or a statically negated list of database names. The acceptable dynamic values are ALL_DATABASES, SYSTEM_DATABASES, and USER_DATABASES. See below examples.

* Example 1: SYSTEM_DATABASES,database1,database2
* Example 2: ALL_DATABASES,-database2,-master
* Example 3: -database2,SYSTEM_DATABASES,USER_DATABASES,-master
* Example 4: SYSTEM_DATABASES,USER_DATABASES
* Example 5: ALL_DATABASES

**@TerminationType**
The controller job can terminate at a specific time or after a specified number of minutes, thereby signaling to the workers to stop processing after finishing their current index. Acceptable values are TIME or DURATION.

**@TerminationDurationMinutes**
This is only valid if the @TerminationType is set to DURATION. The controller job will stop after the number of minutes specified.

**@TerminationTime**
This is only valid if the @TerminationType is set to TIME. The controller job will stop at the specified time. The specified time should be in the format HH:MM.

**@ResetQueue**
If the last time the controller job started it did not finish all the indexes, should the queue be reset or should maintenance resume where the previous iteration left off? Acceptable values are Y or N.

**@HistoryRetentionDays**
How many days of history should be kept in the master.dbo.UpdateIndexesQueue table? NULL value equals no history cleanup and all data will be kept. If a value is set it must be greater than or equal to 1.

**@MinNumberOfPages**
This will control which indexes should be defragmented. Indexes with less than this number of pages will be skipped regardless of fragmentation level. The default for this is 1000 as per Microsoft's recommendation.

**@MaxNumberOfPages**
This will control which indexes should be defragmented. Indexes with greater than this number of pages will be skipped regardless of fragmentation level. The default value for this is NULL.

**@FragmentationLevel1**
This controls if an index will be defragmented or not. Indexes that have a fragmentation percentage less than or equal to this will be skipped for defragmentation. The default for this is 5 as per Microsoft's recommendation.

**@FragmentationLevel2**
This controls if an index's defragmentation will be done via a reorganize or a rebuild. Indexes that have a fragmentation percentage greater than @FragmentationLevel1 and less than or equal to @FragmentationLevel2 will be reorganized. Indexes that have a fragmentation percentage greater than @FragmentationLevel2 will be rebuilt. The default for this is 30 as per Microsoft's recommendation.

**@LOBCompaction**
Should pages be compacted that contain large object columns when reorganizing indexes? Acceptable values are Y or N. The default value for this is Y.

**@SortInTempdb**
Should tempdb be used for sorting during an index rebuild? Acceptable values are Y or N. The default value for this is N.

**@Resumable**
Should an online index rebuild operation be resumable? Acceptable values are Y or N. The default for this is N.

**@MaxDOP**
What is the number of CPUs to use when rebuilding indexes? The default value is NULL (global value will be used).

**@FillFactor**
How full should an index page be filled when rebuilding? The default for this is NULL (global value will be used).

**@NumberOfWorkers**
This controls the number of worker jobs that will be spawned dynamically by the controller job.

## Release Notes ##
**1.4.0**

* Added master.dbo.UpdateIndexesSelectionOverride
* Fixed remove indexes from queue on error rather than leave them in the queue

**1.3.2**

* Added the master.dbo.fn_XMLEscape function
* Added the master.dbo.sp_UpdateIndexesStop stored procedure
* Added more logging to the sp_UpdateIndexesController stored procedure
* Added more logging to the sp_UpdateIndexesWorker stored procedure
* Refactored some code in the sp_UpdateIndexesController stored procedure
* Fixed and issue where if the controller was set to not reset the queue and indexes were left in the queue at the end of the day's run and any of those indexes were removed from the database then the subsequent runs would never remove the deleted indexes from the queue and the job would never really defragment any indexes.
* Fixed issue where XML was not escaped in the event_details column in the UpdateIndexesEvents table.
* Fixed some comment typos

**1.3.1**

* Added a check to see if the index can be defragmented online or not
* Added start and end logging to the controller stored procedure
* Fixed an issue with error logging not saving the error details

**1.3.0**

* Worker jobs are no longer statically created. They are spawned at runtime dynamically by the controller job.
* Cleaned up some SQL to standardize on formatting.

**1.2.0**

* Added history cleanup.

**1.0.2**

* Removed references to deprecated master.dbo.UpdateIndexesQueueHistory table.

**1.0.1**

* Edited query to find highest compatibility level for server.

**1.0.0**

* Initial release.